package school.courses;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;
import school.courses.model.Course;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class SchoolCoursesSpeechlet implements Speechlet {

    public static final String AMAZON_HELP_INTENT = "AMAZON.HelpIntent";

    public static final String SERVUS_INTENT = "ServusIntent";
    public static final String COURSES_CALENDAR_INTENT = "CoursesCalendarIntent";
    public static final String ALL_COURSES_CALENDAR_INTENT = "AllCoursesCalendarIntent";
    public static final String COURSE_CALENDAR_INTENT = "CourseCalendarIntent";
    public static final String COURSE_POINTS_INTENT = "CoursePointsIntent";

    private List<Course> courses = new ArrayList<>();

    @Override
    public void onSessionStarted(final SessionStartedRequest request, final Session session) throws SpeechletException {

        System.out.println("onSessionStarted requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());
    }

    @Override
    public SpeechletResponse onLaunch(final LaunchRequest request, final Session session) throws SpeechletException {

        System.out.println("onLaunch requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());

        return getWelcomeResponse();
    }

    @Override
    public SpeechletResponse onIntent(final IntentRequest request, final Session session) throws SpeechletException {

        System.out.println("onIntent requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());

        Intent intent = request.getIntent();
        String intentName = (intent != null) ? intent.getName() : null;

        System.out.println("Initializing courses");
        courses.clear();
        courses.add(new Course("Mathematik", "Höhere Mathematik",
                LocalDateTime.of(2017, 6, 22, 8, 0),
                new BigDecimal(100), "Nachtexpress", "Albert Einstein"));
        courses.add(new Course("Deutsch", "Germanistik",
                LocalDateTime.of(2017, 6, 26, 10, 30),
                new BigDecimal(100), "Musik Haus", "Thomas Brezina"));
        courses.add(new Course("Mechatronik", "Mechatronik",
                LocalDateTime.of(2017, 6, 23, 15, 45),
                new BigDecimal(100), "Mausefalle", "Optimus Prime"));

        switch (intentName) {
            case SERVUS_INTENT:
                return getServusResponse();
            case COURSES_CALENDAR_INTENT:
                //todo: implement
                return getServusResponse();
            case ALL_COURSES_CALENDAR_INTENT:
                return getWholeCalendarResponse();
            case COURSE_CALENDAR_INTENT:
                //todo: implement
                return getServusResponse();
            case COURSE_POINTS_INTENT:
                //todo: implement
                return getServusResponse();
            case AMAZON_HELP_INTENT:
                return getHelpResponse();
            default:
                throw new SpeechletException("Invalid Intent");
        }
    }

    @Override
    public void onSessionEnded(final SessionEndedRequest request, final Session session) throws SpeechletException {

        System.out.println("onSessionEnded requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());
    }

    private SpeechletResponse getWelcomeResponse() {
        String speechText = "Willkommen beim Lehrveranstaltungs Skill";

        SimpleCard card = new SimpleCard();
        card.setTitle("Lehrveranstaltung");
        card.setContent(speechText);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }

    private SpeechletResponse getServusResponse() {
        String speechText = "Servus";

        SimpleCard card = new SimpleCard();
        card.setTitle("Lehrveranstaltung");
        card.setContent(speechText);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        return SpeechletResponse.newTellResponse(speech, card);
    }

    private SpeechletResponse getWholeCalendarResponse() {
        String speechText = "Ich habe die folgenden Veranstaltungen im System ";

        List<String> speechCourses = new ArrayList<>();
        courses.forEach(course -> {
            speechCourses.add(", " + course.getName());
            speechCourses.add("am");
            speechCourses.add(course.getTime().format(DateTimeFormatter.ofPattern("d MMMM yyyy", Locale.GERMAN)));
            speechCourses.add("um");
            speechCourses.add(course.getTime().format(DateTimeFormatter.ofPattern("H", Locale.GERMAN)) + " Uhr");
            speechCourses.add("mit");
            speechCourses.add(course.getTeacher());
        });
        speechText = speechText + speechCourses.stream().collect(Collectors.joining(" "));

        SimpleCard card = new SimpleCard();
        card.setTitle("Lehrveranstaltung");
        card.setContent(speechText);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        return SpeechletResponse.newTellResponse(speech, card);
    }

    private SpeechletResponse getHelpResponse() {
        String speechText = "Ich bin zurzeit recht dumm, du kannst mich aber intelligenter machen";

        SimpleCard card = new SimpleCard();
        card.setTitle("Lehrveranstaltung");
        card.setContent(speechText);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }
}
