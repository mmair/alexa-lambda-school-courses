package school.courses.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Course {

    private String name;
    private String description;
    private LocalDateTime time;
    private BigDecimal points;
    private String location;
    private String teacher;

    public Course(String name, String description, LocalDateTime time, BigDecimal points, String location, String teacher) {
        this.name = name;
        this.description = description;
        this.time = time;
        this.points = points;
        this.location = location;
        this.teacher = teacher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
