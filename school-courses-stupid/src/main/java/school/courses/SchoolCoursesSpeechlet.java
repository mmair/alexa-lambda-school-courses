package school.courses;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;

public class SchoolCoursesSpeechlet implements Speechlet {

    public static final String AMAZON_HELP_INTENT = "AMAZON.HelpIntent";
    public static final String SERVUS_INTENT = "ServusIntent";

    @Override
    public void onSessionStarted(final SessionStartedRequest request, final Session session) throws SpeechletException {

        System.out.println("onSessionStarted requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());
    }

    @Override
    public SpeechletResponse onLaunch(final LaunchRequest request, final Session session) throws SpeechletException {

        System.out.println("onLaunch requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());

        return getWelcomeResponse();
    }

    @Override
    public SpeechletResponse onIntent(final IntentRequest request, final Session session) throws SpeechletException {

        System.out.println("onIntent requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());

        Intent intent = request.getIntent();
        String intentName = (intent != null) ? intent.getName() : null;

        if (SERVUS_INTENT.equals(intentName)) {
            return getServusResponse();
        } else if (AMAZON_HELP_INTENT.equals(intentName)) {
            return getHelpResponse();
        } else {
            throw new SpeechletException("Invalid Intent");
        }
    }

    @Override
    public void onSessionEnded(final SessionEndedRequest request, final Session session) throws SpeechletException {

        System.out.println("onSessionEnded requestId=" + request.getRequestId() + ", sessionId=" + session.getSessionId());
    }

    private SpeechletResponse getWelcomeResponse() {
        String speechText = "Willkommen zum Alexa Skill Kit";

        SimpleCard card = new SimpleCard();
        card.setTitle("Lehrveranstaltung");
        card.setContent(speechText);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }

    private SpeechletResponse getServusResponse() {
        String speechText = "Servus";

        SimpleCard card = new SimpleCard();
        card.setTitle("Lehrveranstaltung");
        card.setContent(speechText);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        return SpeechletResponse.newTellResponse(speech, card);
    }

    private SpeechletResponse getHelpResponse() {
        String speechText = "Ich bin zurzeit recht dumm, du kannst mich aber intelligenter machen";

        SimpleCard card = new SimpleCard();
        card.setTitle("Lehrveranstaltung");
        card.setContent(speechText);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }
}
