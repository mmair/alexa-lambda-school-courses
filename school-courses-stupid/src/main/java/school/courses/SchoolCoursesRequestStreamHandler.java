package school.courses;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

import java.util.HashSet;

public class SchoolCoursesRequestStreamHandler extends SpeechletRequestStreamHandler {

    public SchoolCoursesRequestStreamHandler() {
        super(new SchoolCoursesSpeechlet(), new HashSet<>());
    }
}
